import { useEffect } from 'react';
const webAppAPI = globalThis.Telegram.WebApp;

const telegramUserDataMock = {
  first_name: 'Test',
  last_name: 'User',

  username: 'testuser',
};

export const useTelegram = () => {
  useEffect(() => {
    globalThis.Telegram?.WebApp?.MainButton.show();
    globalThis.Telegram?.WebApp?.MainButton.enable();
  }, []);

  return {
    webAppAPI,
    queryId: globalThis.Telegram?.WebApp?.initDataUnsafe.query_id,
    userData:
      process.env.NODE_ENV === 'development'
        ? telegramUserDataMock
        : globalThis.Telegram?.WebApp?.initDataUnsafe.user,
  };
};
