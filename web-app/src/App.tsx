import React from 'react';
import UserScore from './modules/UserScore';

import { QueryClient, QueryClientProvider } from 'react-query';

const queryClient = new QueryClient();

export const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <UserScore />
    </QueryClientProvider>
  );
};
