export interface UserScore {
  id: number;
  chatId: string;
  chatName: string;
  score: number;
}

export interface UserData {
  userScores: UserScore[];
}

export interface ScoreInitData {
  userId: number;

  chatId: string;
  chatName: string;
}

export interface UserInitData {
  username: string;
}
