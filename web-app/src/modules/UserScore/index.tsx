import React, { useState, useMemo, useEffect } from 'react';

import Select from 'react-select';

import { css } from '@emotion/react';
import { useMutation, useQuery, useQueryClient } from 'react-query';

import { useTelegram } from '../../hooks/useTelegram';

import Score from '../../components/Score';
import { ScoreInitData, UserData, UserInitData, UserScore as UserScoreType } from './types';

const UserScore = () => {
  const { webAppAPI, userData } = useTelegram();
   const queryClient = useQueryClient();

  const [selectedScoreId, setSelectedScoreId] = useState(null);
  const [currentScore, setCurrentScore] = useState('');

  console.log('webAppAPI', webAppAPI);

  const fullName = `${userData?.first_name} ${userData?.last_name}`;
  const login = userData?.username;

  const { isLoading, isError, data } = useQuery<UserData>(
    'userData',
    async () => {
      const { BOT_SERVER_URL } = process.env;

      const res = await fetch(`${BOT_SERVER_URL}/user/${userData?.username}`);

      return await res.json();
    },
  );
  
  const initUserMutation = useMutation(
    async ({ username }: UserInitData) => {
      const { BOT_SERVER_URL } = process.env;

      await fetch(`${BOT_SERVER_URL}/user`, {
        method: 'POST',
         headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username }),
      });
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('userData');
      }
    }
  );

  const initScoreMutation = useMutation(
    async ({ userId }: ScoreInitData) => {},
    {
      onSuccess: () => {
        queryClient.invalidateQueries('userData');
      },
    },
  );

  const userScoresOptions = useMemo(() => {
    if (data?.userScores) {
      return data.userScores.map(({ id, chatId, chatName }) => ({
        label: chatName ? chatName : chatId,
        value: id,
      }));
    }

    return [];
  }, [data]);

  const scoresByScoreId = useMemo<{ [key: string]: UserScoreType }>(() => {
    if (data) {
      return data.userScores.reduce((acc, current) => {
        acc[current.id] = current;

        return acc;
      }, {});
    }

    return [];
  }, [data]);

  const handleChangeSelectedScore = (selectedOption) => {
    const { value: newSelectedScoreId = null } = selectedOption || {};

    setSelectedScoreId(newSelectedScoreId);

    if (newSelectedScoreId) {
      setCurrentScore(scoresByScoreId[newSelectedScoreId].score.toString());
    }
  };

  const handleChangeScore = (newScore: string) => {
    setCurrentScore(newScore);
  };

  const selectedOption = useMemo(
    () => userScoresOptions.find(({ value }) => value === selectedScoreId),
    [userScoresOptions],
  );

  // useEffect(
  //   () => {
  //     if (!data && userData) {
  //       initUserMutation.mutate({ username: userData?.username });
  //     }
  //   },
  //   [data, userData]
  // );

  useEffect(
    () => {
      if (userData) {
        // const foundCurrentScore = userData.
      }
    },
    [userData],
  )

  if (isLoading) {
    return (
      <div>Loading...</div>
    );
  }

  if (isError) {
    return (
      <div>Sorry, something wrong happened</div>
    );
  };

  return (
    <div
      css={css`
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
      `}
    >
      <h1 id='user-name-heading'>{fullName}</h1>

      <h3 id='user-login'>@{login}</h3>

      <Select
        value={selectedOption}
        onChange={handleChangeSelectedScore}
        options={userScoresOptions}
      />

      {selectedScoreId && (
        <Score
          key={selectedScoreId}
          onChange={handleChangeScore}
          currentScore={currentScore}
          scoreData={scoresByScoreId[selectedScoreId]}
        />
      )}
    </div>
  );
};

export default UserScore;
