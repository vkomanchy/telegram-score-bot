import { UserScore } from '../../modules/UserScore/types';

export interface ScoreProps {
  scoreData: UserScore;
  currentScore: string;
  onChange: (value: string) => void;
}

export interface ScoreMutationProps {
  score: number;
}
