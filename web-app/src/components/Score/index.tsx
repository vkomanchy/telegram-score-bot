import React, { useState } from 'react';
import { ScoreMutationProps, ScoreProps } from './types';
import { useMutation, useQueryClient } from 'react-query';
import { useTelegram } from '../../hooks/useTelegram';

const Score: React.FC<ScoreProps> = ({ scoreData, onChange, currentScore }) => {
  const queryClient = useQueryClient();
  const { queryId } = useTelegram();

  const [isInputVisible, setIsInputVisible] = useState(false);

  const scoreMutation = useMutation(
    async ({ score }: ScoreMutationProps) => {
      const { BOT_SERVER_URL } = process.env;

      await fetch(`${BOT_SERVER_URL}/score/${scoreData.id}/set-score`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ score, queryId }),
      });
    },
    {
      onSuccess: () => {
        queryClient.invalidateQueries('userData');
      },
    },
  );

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    onChange(value);
  };

  const toggleInputVisibility = () => {
    setIsInputVisible((prev) => !prev);
  };

  const handleSaveScore = () => {
    if (isNaN(Number(currentScore))) {
      alert('Not a number!');
    } else {
      scoreMutation.mutate({ score: Number(currentScore) });

      toggleInputVisibility();
    }
  };

  if (scoreMutation.isLoading) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      {isInputVisible ? (
        <>
          <input
            name='score'
            type='string'
            value={currentScore}
            onChange={handleChange}
          />

          <button onClick={handleSaveScore}>Save</button>
        </>
      ) : (
        <>
          Current score:
          {scoreData.score}
          <button onClick={toggleInputVisibility}>Change score</button>
        </>
      )}
    </div>
  );
};

export default Score;
