import { Sequelize } from 'sequelize';
const path = require('path');

const databasePath = path.resolve('build/database', 'score-bot-database.sql');

const sequelize = new Sequelize({
  dialect: 'sqlite',
  storage: databasePath,
});

module.exports = {
  sequelize,
};
