import { DataTypes } from 'sequelize';

const { sequelize } = require('./index');

const User = sequelize.define('User', {
  username: {
    type: DataTypes.STRING,
    unique: true,
  },
});

module.exports = {
  User,
};
