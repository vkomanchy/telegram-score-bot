import { DataTypes } from 'sequelize';

const { sequelize } = require('./index');

const Score = sequelize.define('Score', {
  chatId: DataTypes.STRING,
  chatName: DataTypes.STRING,
  score: DataTypes.NUMBER,
});

module.exports = {
  Score,
};
