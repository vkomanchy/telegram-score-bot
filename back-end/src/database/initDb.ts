const { Score } = require('./score');
const { User } = require('./user');

User.hasMany(Score);
Score.belongsTo(User);
