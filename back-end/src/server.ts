import Application from 'koa';

const Koa = require('koa');
const logger = require('koa-logger');
const cors = require('@koa/cors');
const { router } = require('./controllers');

const initKoaServer = (bot: any) => {
  const app: Application = new Koa();

  app.context.bot = bot;

  app.use(logger());
  app.use(cors({
    'Access-Control-Allow-Origin': 'http://localhost:1234',
  }));

  app.use(router.routes()).use(router.allowedMethods());

  app.listen(3000);
  console.log('Server listen on port 3000');
};

module.exports = {
  initKoaServer,
};
