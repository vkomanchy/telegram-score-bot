export interface UserData {
  username: string;
}

export interface DataWithQueryId {
  queryId: string;
}

export interface ScoreData extends DataWithQueryId {
  chatId: string;
  chatName: string;
  score: number;
  userId: number;
}

export interface ScoreFieldData extends DataWithQueryId {
  score: number;
}
