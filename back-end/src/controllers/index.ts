const Router = require('@koa/router');
const { injectScoreRoutes } = require('./score');
const { injectUserRoutes } = require('./user');

const router = new Router();

injectUserRoutes(router);
injectScoreRoutes(router);

module.exports = {
  router,
};
