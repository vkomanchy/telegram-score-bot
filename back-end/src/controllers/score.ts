import type Router from '@koa/router';
import { ScoreData, ScoreFieldData } from './types';

const { User } = require('../database/user');
const { Score } = require('../database/score');

const { authMiddleware } = require('../middlewares/auth');

const { koaBody } = require('koa-body');

const injectScoreRoutes = (router: Router) => {
  router
    .get('/score/:id', authMiddleware, async (ctx) => {
      const { id } = ctx.params;

      if (!id) {
        ctx.status = 400;

        return (ctx.body = 'score id not found in params');
      }

      const foundScore = await Score.findOne({
        where: {
          id,
        },
      });

      if (!foundScore) {
        ctx.status = 404;
        return (ctx.body = 'Score not found');
      }

      ctx.body = foundScore;
    })
    .post('/score', koaBody(), async (ctx) => {
      const bodyAsScoreData = ctx.request.body as ScoreData;

      if (
        !bodyAsScoreData ||
        !bodyAsScoreData.chatId ||
        !bodyAsScoreData.score
      ) {
        ctx.status = 400;

        return (ctx.body = 'chatId or score not defined');
      }

      const { chatId, chatName, score, userId, queryId } = bodyAsScoreData;
      
      console.log({ queryId })

      const user = await User.findOne({
        where: {
          id: userId,
        },
      });

      const newScore = await user.createScore({ chatId, chatName, score });

      ctx.body = newScore;
    })
    .put('/score/:id', koaBody(), async (ctx) => {
      const { id } = ctx.params;
      const bodyAsScoreData = ctx.request.body as ScoreData;

      if (
        !bodyAsScoreData ||
        !bodyAsScoreData.chatId ||
        !bodyAsScoreData.score
      ) {
        ctx.status = 400;

        return (ctx.body = 'chatId or score not defined');
      }

      if (!id) {
        ctx.status = 400;

        return (ctx.body = 'score id not found in params');
      }

      const foundScore = await Score.findOne({
        where: {
          id,
        },
      });

      if (!foundScore) {
        ctx.status = 404;

        return (ctx.body = 'Not found score with such id');
      }

      const updatedScore = await foundScore.update(bodyAsScoreData);

      return (ctx.body = updatedScore);
    })
    .patch('/score/:id/set-score', koaBody(), async (ctx) => {
      const { id } = ctx.params;
      const bodyAsScoreData = ctx.request.body as ScoreFieldData;

      console.log('bodyAsScoreData', bodyAsScoreData, bodyAsScoreData.score, bodyAsScoreData.queryId)
      
      const data = await ctx.bot.answerWebAppQuery(bodyAsScoreData.queryId, {
        type: 'article',
        id: bodyAsScoreData.queryId,
        title: 'Success !!!',
        input_message_content: {
          message_text: 'HELLO HOW ARE YOU',
        },
      });

      console.log('data', data);

      if (
        !bodyAsScoreData ||
        !bodyAsScoreData.score
      ) {
        ctx.status = 400;

        return (ctx.body = 'score not defined');
      }

      if (!id) {
        ctx.status = 400;

        return (ctx.body = 'score id not found in params');
      }

      const foundScore = await Score.findOne({
        where: {
          id,
        },
      });

      if (!foundScore) {
        ctx.status = 404;

        return (ctx.body = 'Not found score with such id');
      }

      const updatedScore = await foundScore.update(bodyAsScoreData);

      return (ctx.body = updatedScore);
    })
    .delete('/score/:id', async (ctx) => {
      const { id } = ctx.params;

      if (!id) {
        ctx.status = 400;

        return (ctx.body = 'score id not found in params');
      }

      await Score.destroy({
        where: {
          id,
        },
      });

      ctx.body = 'Deleted';
    });
};

module.exports = {
  injectScoreRoutes,
};
