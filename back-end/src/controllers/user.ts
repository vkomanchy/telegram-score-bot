import type Router from '@koa/router';
import type { UserData } from './types';

const { koaBody } = require('koa-body');

const { Score } = require('../database/score');
const { User } = require('../database/user');

const usernameErrorMessage = "Request doesn't contain username";

const injectUserRoutes = (router: Router) => {
  router
    .get('/user/:username', async (ctx, next) => {
      const { username } = ctx.params;

      if (!username) {
        ctx.status = 400;

        return (ctx.body = usernameErrorMessage);
      }

      const foundUser = await User.findOne(
        {
          where: {
            username,
          },
        },
      );

      
      if (!foundUser) {
        ctx.status = 404;
        return (ctx.body = 'User not found');
      }

      const userScores = await foundUser.getScores();

      ctx.body = {
        ...foundUser.toJSON(),
        userScores,
      };
    })
    .post('/user', koaBody(), async (ctx, next) => {
      const bodyAsUserData = ctx.request.body as UserData;

      if (!bodyAsUserData || !bodyAsUserData.username) {
        ctx.status = 400;

        return (ctx.body = usernameErrorMessage);
      }

      const { username } = bodyAsUserData;

      const newUser = await User.create({ username });

      ctx.body = newUser;
    });
};

module.exports = {
  injectUserRoutes,
};
