import type { Message } from 'node-telegram-bot-api';

const TelegramBot = require('node-telegram-bot-api');

const initBot = () => {
  const { BOT_TOKEN, NODE_ENV } = process.env;

  const bot = new TelegramBot(BOT_TOKEN, {
    polling: true,
    // testEnvironment: NODE_ENV === 'development',
  });

  bot.onText(/\/echo (.+)/, (msg: Message, match: string[]) => {
    const chatId = msg.chat.id;
    console.log('msg', JSON.stringify(msg));
    const resp = match[1];

    bot.sendMessage(chatId, `Changed ${resp}`);
  });

  bot.on('message', async (msg: Message) => {
    const {
      chat: { id: chatId },
      text,
    } = msg;

    console.log('msg', JSON.stringify(msg));

    if (text === '/start') {
      bot.sendMessage(chatId, 't.me/ScoreSomethingBot/scoresomething');
    }

    if (text === '/buttons') {
      bot.sendMessage(chatId, 'Button', {
        reply_markup: {
          inline_keyboard: [
            [
              {
                text: 'Заполнить форму',
                web_app: { url: 'https://telegram-score-bot.surge.sh/' },
              },
            ]
          ],
        },
      });
    }
  });

  return bot;
};

module.exports = {
  initBot,
};
