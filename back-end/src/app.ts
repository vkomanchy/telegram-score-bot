require('dotenv').config();

require('./database/initDb');
const { initBot } = require('./bot');
const { initKoaServer: initKoaServerFunc } = require('./server');
const { sequelize } = require('./database');

const bot = initBot();
initKoaServerFunc(bot);

sequelize.sync();
