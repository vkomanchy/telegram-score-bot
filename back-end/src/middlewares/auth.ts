import Application from 'koa';

const { checkWebAppSignature } = require('./authHelpers');

const authMiddleware = async (ctx: Application.Context, next: Application.Next) => {
  const { initData } = ctx.query;

  const isValid = await checkWebAppSignature(initData);

  if (isValid) {
    return next();
  }

  ctx.status = 401;
  return ctx.body = 'Not authorized';
};

module.exports = {
  authMiddleware,
};
