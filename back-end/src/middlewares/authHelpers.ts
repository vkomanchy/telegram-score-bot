const cryptoJS = require('crypto-js');

const checkWebAppSignature = async (telegramInitData: string) => {
  if (process.env.NODE_ENV === 'development') {
    console.warn('process.env.NODE_ENV === development, don\'t use on production');

    return true;
  }

  const initData = new URLSearchParams(telegramInitData);
  const hash = initData.get('hash');

  const dataToCheck: string[] = [];

  initData.sort();
  initData.forEach(
    (val, key) => key !== 'hash' && dataToCheck.push(`${key}=${val}`),
  );

  const secret = cryptoJS.HmacSHA256(process.env.BOT_TOKEN || '', 'WebAppData');
  const _hash = cryptoJS.HmacSHA256(dataToCheck.join('\n'), secret).toString(
    cryptoJS.enc.Hex,
  );

  return _hash === hash;
};

module.exports = {
  checkWebAppSignature,
};
